# LaTeX docker image
This repository uses GitLab CI to automatically build a Docker Image with Latexmk and all Texlive packages, and also includes make and inkscape for svg to pdf image conversion.
This is useful if you want to easily build LaTeX repositories hosted on GitLab!

The resulting image is `registry.gitlab.com/trenta3/latex-docker:master`, and can be used in common GitLab CI jobs as:
```yaml
build-latex:
  image: registry.gitlab.com/trenta3/latex-docker:master
  script:
    - latexmk -lualatex main.tex
  artifacts:
    paths:
      - main.pdf
```
