FROM fedora:35

WORKDIR /latex
RUN dnf install -y \
    https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
    https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
RUN dnf update -y && \
    dnf install -y make latexmk inkscape && \
    dnf install -y ffmpeg x264 x265 fdkaac && \
    dnf install -y texlive-* && \
    dnf clean all
